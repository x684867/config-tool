#!/usr/bin/env python



def read_version(fname):
	print "reading version"
	with open(fname,'r') as f:
		v=f.read()
		print "\tcurrent version:{}".format(v)
		return v

def write_version(fname,v):
	print "file:{}",format(fname)
	with open(fname,'w') as f:
		print "\tnew version:{}".format(v)
		f.write("{}".format(v))

def strVersion2int(stringVersion):
	return [int(x) for x in stringVersion.split('.')]

def intVersion2str(intVersion):
	return ".".join(str(x) for x in intVersion)

def bumpVersion(intVersion):
	print "\tincrementing version with rollover at 99"
	for i in range(len(intVersion)-1,-1,-1):
		if intVersion[i] and (i>0) == 99:
			intVersion[i]=0
			intVersion[i-1]+=1
		else:
			intVersion[i]+=1
			return intVersion
	raise("bumpVersion failed to bump the input version")


if __name__ == "__main__":

	fname="VERSION.txt"

	print "bumping version."
	write_version(
		fname,
		intVersion2str(
			bumpVersion(
				strVersion2int(
					read_version(fname)
				)
			)
		)
	)
	print "have a nice day.  Version is bumped."
